
var app = {
  initialize: function () {
	  this.bindEvents();
  },
  bindEvents: function () {
	  document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  onDeviceReady: function () {
	  var notificationOpenedCallback = function(jsonData) {
			alert(JSON.stringify(jsonData));
		};

		window.plugins.OneSignal.init("5c8ac299-2c66-4d94-9790-53e0a67c4694",
                                {googleProjectNumber: "1043931002511"},
                                 notificationOpenedCallback);
  
		window.plugins.OneSignal.setSubscription(true);
  
  // Show an alert box if a notification comes in when the user is in your app.
		window.plugins.OneSignal.enableInAppAlertNotification(true);
		
		navigator.notification.alert(
		  "You are on page '" + document.title + "'",                 // message
		  function () { console.log('Alert callback called!'); }, // callback
		  'Message',         // title
		  'Done'             // buttonName
	  );
  },
};

app.initialize();
